Provide AppCenter/Test upload command & path to IPA.
AppCenter_Config='appcenter test run appium --app 'APP_OWN' --devices 'DEVICE_SET'
app_path='APP_PATH'
project_path='*../target/upload'
test_series='development'

# Run using the command "sh upload.sh"
Build_TestUpload_Command='mvn -DskipTests -P prepare-for-upload package'
echo $Build_TestUpload_Command
eval $Build_TestUpload_Command

#echo $APPCENTER_SOURCE_DIRECTORY/packages/Xamarin.UITest.*/tools
$AppCenter_Config --app-path $app_path --test-series $test_series --locale en_US --build-dir $project_path --async
