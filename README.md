

# Template UI Test


# Appium Setup for iOS



ขั้นตอนการตั้งค่า และติดตั้งสำหรับ run automated UI Test ซึ่ง ****appium**** จะแบ่งเป็น 2 ส่วนหลักๆ คือ

1. Appium Desktop

2. Appium Client โดยเป็นคำสั่ง WebDriver เพื่อใช้ในการ run อุปกรณ์

# Installing


**1.  Install node**

``` ruby
$brew install node
```


**2. Install Xcode**

**3. Install Xcode command line tool (กรณีมี Xcode หลายเวอร์ชั่น)**

``` ruby
$xcode-select --install
```
**4. Install carthage**

``` ruby
$brew install carthage
```

**5. ติดตั้ง Appium**

``` ruby

$sudo npm install -g appium
$appium -v //เช็คเวอร์ชั่น

$sudo npm install appium-doctor -g

$appium-doctor //เช็คเวอร์ชั่น ตรวจสอบ Tool ที่จำเป็นสำหรับ run Appium
หรือ
$appium-doctor --ios
```

install lib เพิ่มเติม
``` ruby
$brew install libimobiledevice
$npm install -g ios-deploy
```

**6. Install IDE** (VS Code, Eclipse, Android studio หรืออื่นๆ ) ตาม ****Appium Client**** ที่เราจะใช้

**7. ติดตั้ง Appium Desktop** [Release 1.21.0 · appium/appium-desktop · GitHub](https://github.com/appium/appium-desktop/releases/tag/v1.21.0)

**8. เปิด Appium Desktop ขึ้นมา แล้วกด Start Server**



## Preparing your app for test with Appium Client  (java)

ขั้นตอนนี้จะเป็นการสร้าง WebDriver ไว้ทดสอบ Test script ตาม Test Case ที่ต้องการจะทดสอบของแอปพลิเคชันนั้นๆ โดย ซึ่งการสร้างโปรเจ็คจะรองรับไปถึงการนำไปทดสอบบน App Center ด้วย

สร้างโปรเจค Java เลือกเป็น Maven เพิ่ม Dependency เข้าไปที่ไฟล์ pom.xml

> io.appium จะเป็น Framework ที่เราใช้ทดสอบ
> testng สำหรับดูผลลัพธ์การทดสอบ และขั้นตอนการ Test ต่างๆ

``` xml
<!-- https://mvnrepository.com/artifact/io.appium/java-client -->
<dependency>
    <groupId>io.appium</groupId>
    <artifactId>java-client</artifactId>
    <version>7.5.1</version>
</dependency>
<dependency>
    <groupId>junit</groupId>
    <artifactId>junit</artifactId>
    <version>4.12</version>
</dependency>
<dependency>
    <groupId>com.microsoft.appcenter</groupId>
    <artifactId>appium-test-extension</artifactId>
    <version>1.5</version>
</dependency>
```

  สร้างไฟล์ Config เพื่อเลือก Application, Device หรืออื่นๆ ที่ต้องการจะทดสอบ

``` java
public static EnhancedIOSDriver<IOSElement> startApp() throws MalformedURLException {
    DesiredCapabilities capabilities = new DesiredCapabilities();

    capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "ios");
    capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 7");
    capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
    capabilities.setCapability(MobileCapabilityType.NO_RESET, false);

    //PATH_APP (file name .app, .ipa)    
    String ipaPath = "PATH_NAME";
    capabilities.setCapability(MobileCapabilityType.APP, ipaPath); 

    //(Real Device)
    capabilities.setCapability(MobileCapabilityType.UDID, "DEVICE_UDID");
    
    URL url = new URL("http://localhost:4723/wd/hub");
    
    return Factory.createIOSDriver(url, capabilities);
}
```

ตัวอย่าง  Class Test Script

``` java
package  com.microsoft.utils;

import  com.microsoft.appcenter.appium.Factory;
import  com.microsoft.appcenter.appium.MemoryEventReporter;
import  com.microsoft.appcenter.appium.EnhancedIOSDriver;

import  io.appium.java_client.ios.IOSElement;
import  io.appium.java_client.remote.MobileCapabilityType;

import  org.openqa.selenium.remote.DesiredCapabilities;
import  org.openqa.selenium.Keys;

import  java.net.MalformedURLException;
import  java.net.URL;

import  org.junit.rules.TestWatcher;
import  org.junit.Rule;
import  org.junit.After;
import  org.junit.Before;
import  org.junit.Test;

public class MainSetupTest {
    public EnhancedIOSDriver<IOSElement> driver;
    public MemoryEventReporter reporter = new MemoryEventReporter();

    @Rule
    public TestWatcher watcher = Factory.createWatcher();

    public static EnhancedIOSDriver<IOSElement> startApp() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "ios");
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 7");
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
        capabilities.setCapability(MobileCapabilityType.NO_RESET, false);

        //(Device)
        capabilities.setCapability(MobileCapabilityType.UDID, "DEVICE_UDID");
        String ipaPath = "PATH_NAME";
        capabilities.setCapability(MobileCapabilityType.APP, ipaPath);

        URL url = new URL("http://localhost:4723/wd/hub");

        return Factory.createIOSDriver(url, capabilities);
    }

    @Before
    public void setupAppium() throws MalformedURLException {
        driver = startApp();
        driver.label("App Launched");
    }

    @After
    public void after() throws Exception {
        if (driver != null) {
            driver.quit();
        }
    }

    @Test
    public void TC_1_TEST() throws InterruptedException {
        MobileElement input = driver.findElementByAccessibilityId("XXXX");
    }
}
```

เมื่อ setup เสร็จเรียบร้อยแล้วให้ลองทดสอบ Run
ถ้าสำเร็จจะมี Device และพร้อมเปิด app ที่เราตั้งค่าไว้ใน DesiredCapabilities ขึ้นมา


## Additional for iOS (Real Device)

ขั้นตอนการ setup สำหรับ run บน อุปกรณ์จริง

**1. ลง ideviceinstaller**
``` ruby
$brew install ideviceinstaller
```
**2. จะต้องมี WebDriverAgent**

สำหรับ Appium Desktop

``` ruby
$ cd /Applications/Appium.app/Contents/Resources/app/node_modules/appium/node_modules/appium-webdriveragent
```

สำหรับ ลงผ่าน Node

``` ruby
$cd /usr/local/lib/node_modules/appium/node_modules/appium-webdriveragent
```

**3.เมื่อเข้าไปใน folder appium-webdriveragent แล้วให้รันคำสั่งด้านล่าง**

``` ruby
$mkdir -p Resources/WebDriverAgent.bundle
```

*** กรณี appium ต่ำกว่าเวอร์ชั่น 1.20.0
 ``` ruby
  $./Scripts/bootstrap.sh -d
  ```

**4. เปิดโปรเจค  WebDriverAgent.xcodeproj ขึ้นมา**

**5. ตั้งค่า Profile และ Certificate สำหรับ run บนอุปกรณ์ สามารถดูเพิ่มเติมได้ที่ [link](https://appium.io/docs/en/drivers/ios-xcuitest-real-devices/#basic-manual-configuration)**

หลังจากตั้งค่าตามไฟล์เสร็จเรียบร้อยแล้ว ลองรันตามคำสั่งด้านล่างเพื่อตรวจสอบว่าไฟล์ที่เราตั้งค่านั้นถูกต้อง

``` ruby
$xcodebuild -project WebDriverAgent.xcodeproj -scheme WebDriverAgentRunner -allowProvisioningUpdates -destination 'id=DEVICE_UDID' test
```

>  ****_Note:_**** _สำหรับทดสอบ Config ว่าสามารถ run ได้แล้วบนอุปกรณ์จริง_

**กรณี Error: 65 or Null ให้ตรวจสอบ WebDriverAgent ว่าเราลงถูก path หรือไม่

## Prepare command line for App Center

setup command line เพื่อที่จะใช้คำสั่งนี้ บน cloud, App Center หรือการทดสอบแบบเลือก case by case 

**1. Install maven**

``` ruby
$brew install maven
```

**2. Set path  JAVA_HOME** (Big Sur)

สำหรับการตั้งค่า path ขึ้นอยู่กับ macOS  [เพิ่มเติมสำหรับ OS อื่นๆ](https://mkyong.com/java/how-to-set-java_home-environment-variable-on-mac-os-x/)

``` ruby
$nano ~/.zshenv

เพิ่ม path
export JAVA_HOME=$(/usr/libexec/java_home)
```
ถ้าเสร็จเรียบร้อยแล้วจะสามารถเรียกไฟล์ cmd ``` $mvn -version```

ผลลัพธ์
``` ruby
Apache Maven 3.8.1 (05c21c65bdfed0f71a2f2ada8b84da59348c4c5d)
Maven home: /usr/local/Cellar/maven/3.8.1/libexec
Java version: 11.0.11, vendor: AdoptOpenJDK, runtime: /Library/Java/JavaVirtualMachines/adoptopenjdk-11.jdk/Contents/Home
Default locale: en_TH, platform encoding: UTF-8
OS name: "mac os x", version: "11.2", arch: "x86_64", family: "mac"
```

**3. เพิ่ม Profile ไปในไฟล์ pom.xml**

กรณีถ้าต้องการอัพโหลดไฟล์ไปยัง App Center

``` xml
<profile>
  <id>prepare-for-upload</id>
  <build>
      <plugins>
          <plugin>
              <groupId>org.apache.maven.plugins</groupId>
              <artifactId>maven-dependency-plugin</artifactId>
              <version>2.10</version>
              <executions>
                  <execution>
                      <id>copy-dependencies</id>
                      <phase>package</phase>
                      <goals>
                          <goal>copy-dependencies</goal>
                      </goals>
                      <configuration>
                          <outputDirectory>${project.build.directory}/upload/dependency-jars/</outputDirectory>
                          <useRepositoryLayout>true</useRepositoryLayout>
                          <copyPom>true</copyPom>
                          <addParentPoms>true</addParentPoms>
                      </configuration>
                  </execution>
              </executions>
          </plugin>
          <plugin>
              <groupId>org.apache.maven.plugins</groupId>
              <artifactId>maven-help-plugin</artifactId>
              <version>2.2</version>
              <executions>
                  <execution>
                      <id>generate-pom</id>
                      <phase>package</phase>
                      <goals>
                          <goal>effective-pom</goal>
                      </goals>
                      <configuration>
                          <output>${project.build.directory}/upload/pom.xml</output>
                      </configuration>
                  </execution>
              </executions>
          </plugin>
          <plugin>
              <groupId>org.apache.maven.plugins</groupId>
              <artifactId>maven-resources-plugin</artifactId>
              <executions>
                  <execution>
                      <id>copy-testclasses</id>
                      <phase>package</phase>
                      <goals>
                          <goal>testResources</goal>
                      </goals>
                      <configuration>
                          <outputDirectory>${project.build.directory}/upload/test-classes</outputDirectory>
                          <resources>
                              <resource>
                                  <directory>
                                      ${project.build.testOutputDirectory}
                                  </directory>
                              </resource>
                          </resources>
                      </configuration>
                  </execution>
              </executions>
          </plugin>
      </plugins>
  </build>
</profile>
```

**4. Run test**

Run test on Local

``` ruby
$mvn test //All Test 
$mvn -Dtest=CLASS_NAME test #//จะใช้ Class Test 

or
$mvn -Dtest=CLASS_NAME#METHOD_NAME //เลือกทดสอบตาม Class และ Method
```

Run test on App Center

1. Install App Center cli ก่อน และหลังจากนั้น Login 
``` ruby
$npm install -g appcenter-cli #กรณียังไม่เคยลง
$appcenter login #จะเปิด browser เพื่อให้เข้าสุ่ระบบ และนำ token กลับมากรอก 

$mvn -DskipTests -P prepare-for-upload package #เตรียม script สำหรับ ทดสอบบน App Center

$appcenter test run appium --app "APP_OWN" --devices "DEVICE_SET" --app-path 'PATH_IPA' --test-series "TEST_SERIES" --locale "en_US" --build-dir '*../target/upload'
```

>  ****APP_OWN**** _ชื่อโปรเจ็คที่อยู่บน App Center (สามารถคัดลอกมาจาก App Center ตอนสร้าง New run test ขั้นตอน Submit)_
>  
>  ****DEVICE_SET**** _อุปกรณ์ที่ต้องการจะทดสอบ_
>
>  ****PATH_IPA**** _ที่อยู่ไฟล์  IPA ที่อยู่บนเครื่องเรา เช่น Download, Desktop_
>
>  ****TEST_SERIES**** _ชื่อ test ที่ตั้งใน App Center Console แนะนำให้ตั้งเป็นชื่อ Branch_
>
>  ****--build-dir****   _ที่อยู่ไฟล์  target/upload  
<br>

ซึ่งจากโปรเจ็ค Template_UI_TEST สามารถเปลี่ยนข้อมูลต่างๆ ข้างต้นได้ที่ไฟล์  config    ```upload.sh``` และรันคำสั่ง     ```$sh upload.sh``` ได้เลย

*ข้อมูลเพิ่มเติม*
[มือใหม่หัดใช้ Appium : Episode 0 — Introduction ! | by Prathan D. | WeLoveBug dot Com](https://welovebug.com/%E0%B8%A1%E0%B8%B7%E0%B8%AD%E0%B9%83%E0%B8%AB%E0%B8%A1%E0%B9%88%E0%B8%AB%E0%B8%B1%E0%B8%94%E0%B9%83%E0%B8%8A%E0%B9%89-appium-episode-0-introduction-fbd540ce99d)
[XCUITest Real Devices (iOS) - Appium](https://appium.io/docs/en/drivers/ios-xcuitest-real-devices/#basic-manual-configuration)
[Appium Beginner Tutorial 15 | How to setup iOS Automation on Mac OS - YouTube](https://www.youtube.com/watch?v=-_6C_-CMqSk)
วิธีตั้งค่า Path JAVA_HOME  [How to Set $JAVA_HOME environment variable on macOS - Mkyong.com](https://mkyong.com/java/how-to-set-java_home-environment-variable-on-mac-os-x/)
[Maven Surefire Plugin – Using TestNG (apache.org)](https://maven.apache.org/surefire/maven-surefire-plugin/examples/testng.html)


@Saowalak Rungrat
