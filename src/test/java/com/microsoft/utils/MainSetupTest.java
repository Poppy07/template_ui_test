package com.microsoft.utils;

import com.microsoft.appcenter.appium.Factory;
import com.microsoft.appcenter.appium.MemoryEventReporter;
import com.microsoft.appcenter.appium.EnhancedIOSDriver;

import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.ios.IOSStartScreenRecordingOptions;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.interactions.touch.TouchActions;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

import java.io.File;
import org.apache.commons.io.FileUtils;
import org.apache.commons.codec.binary.Base64;

import org.junit.rules.TestWatcher;
import org.junit.Rule;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import static org.junit.Assert.fail;

public class MainSetupTest {
    public static EnhancedIOSDriver<MobileElement> driver;
    private Dimension windowSize;

    @Rule
    public TestWatcher watcher = Factory.createWatcher();

    public static EnhancedIOSDriver<MobileElement> startApp() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "PLATFORM_NAME");
        // capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "14.4");
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "DEVICE_NAME");
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
        capabilities.setCapability(MobileCapabilityType.NO_RESET, false);
    
        //(Device)
        capabilities.setCapability(MobileCapabilityType.UDID, "DEVICE_UDIO");
        String ipaPath = "PATH_IPA";  // *../Users/saowalakrungrat/Desktop/RoyalCanin_STG.ipa
        capabilities.setCapability(MobileCapabilityType.APP, ipaPath);

        URL url = new URL("http://localhost:4723/wd/hub");

        return Factory.createIOSDriver(url, capabilities);
    }

    @Before
    public void setupAppium() throws MalformedURLException {
        driver = startApp();
        driver.label("App Launched");

        //อัด VDO ก่อนเริ่มทอดสอบ
        //startRecordingScreen();
    }

    @After
    public void after() throws Exception {
        if (driver != null) {
            //หยุดการอัด VDO หลังทดสอบเสร็จ หรือ Error
            //stopRecordScreen();

            driver.label("Stopping App");
            driver.quit();
        }
    }

    public static EnhancedIOSDriver<MobileElement> getDriver() {
        return driver;
    }

    /************************************/
    /************************************/
    /**         Helper Function        **/
    /************************************/
    /************************************/

    // Action
    
    // หา Element
    //id -> element id (accessibilityId) ของ object ที่ต้องการ
    public static MobileElement findElementByID(String id) {
        System.out.println("Finding element ID: " + id);
        try {
            MobileElement element = driver.findElementByAccessibilityId(id);
            return element;
        } catch (Exception e){
            System.out.println("Can't Find element ID: " + id);
            fail("Can't Find element ID: " + id);
        }
        return null;
    }

    //กรอกข้อมูล
    //id -> element id (accessibilityId) ของ object ที่ต้องการ
    //value -> ค่าที่ต้องการจะใส่ลงไป
    public void inputTextField(String id, String value) throws InterruptedException {
        try {
            MobileElement input = findElementByID(id);
            input.clear();
            input.sendKeys(value, Keys.ENTER);
        } catch (Exception e){
            fail("Fail something input: " + id);
        }
        delay(200);
    }

    //กดปุ่ม ตาม Element by ID
    //id -> element id (accessibilityId) ของ object ที่ต้องการ
    public void pressedButtonByID(String id) throws InterruptedException {
        MobileElement login = findElementByID(id);
        login.click();
        delay(3000);
    }

    //กดปุ่ม ตาม position ที่กำหนด
    //x -> ตำแหน่ง X ของหน้าจอ
    //y -> ตำแหน่ง Y ของหน้าจอ
    public void pressedByPosition(Integer x, Integer y) throws InterruptedException {
        try {
            TouchAction touchAction = new TouchAction(driver);
            touchAction.tap(PointOption.point(x, y)).perform();
        }catch (Exception e){
            fail(e.getMessage());
        }
    }

    //check text
    //id -> element id (accessibilityId) ของ object ที่ต้องการ
    //expectStr -> ค่าที่ถูกต้อง ที่ต้องการให้แสดงผลลัพธ์
    public void checkTextByID(String id, String expectStr) {
        String answer = findElementByID(id).getText();
        Assert.assertEquals("Failure - strings are not equal", expectStr, answer);
    }

    //check ปุ่ม disabled 
    //id -> element id (accessibilityId) ของ object ที่ต้องการ
    public void checkButtonDisabled(String id) {
        try {
            driver.findElement(By.id(id)).isEnabled();
        } catch (Exception e) {
            //Assert.assertTrue(false);
        }
    }

    //Check show element
    //id -> element id (accessibilityId) ของ object ที่ต้องการ
    public void checkShowElement(String id) {
        try {
            MobileElement element = findElementByID(id);
            Assert.assertTrue(element.isDisplayed());
            delay(3000);
        } catch (Exception e){
            //Reporter.log(e.toString());
        }
    }

    //Swipe by percentages เลื่อนหน้าจอ บน ล่าง ซ้าย ขวา
    /**
    dir -> direction of swipe: UP, DOWN, LEFT, RIGHT
    anchorPercentage -> Percent of screen
    duration -> ความเร็วในการเลื่อน หน่วย มิลลิวินาที
    **/
    public static void scrollByPercentages(Direction dir, double anchorPercentage, int duration) throws Exception {
        Dimension size = driver.manage().window().getSize();
        double startPercentage = 0.8;
        double endPercentage = 0.2;
        int anchor;
        PointOption pointOptionStart, pointOptionEnd;

        switch (dir) {
            case DOWN: // from up to down
                anchor = (int) (size.width * anchorPercentage);
                pointOptionStart = PointOption.point(anchor, (int) (size.height * endPercentage));
                pointOptionEnd = PointOption.point(anchor, (int) (size.height * startPercentage));
                break;
            case UP: // from down to up
                anchor = (int) (size.width * anchorPercentage);
                pointOptionStart = PointOption.point(anchor, (int) (size.height * startPercentage));
                pointOptionEnd = PointOption.point(anchor, (int) (size.height * endPercentage));
                break;
            case LEFT: // from right to left
                anchor = (int) (size.height * anchorPercentage);
                pointOptionStart = PointOption.point((int) (size.width * startPercentage), anchor);
                pointOptionEnd = PointOption.point((int) (size.width * endPercentage), anchor);
                break;
            case RIGHT: // from left to right
                anchor = (int) (size.height * anchorPercentage);
                pointOptionStart = PointOption.point((int) (size.width * endPercentage), anchor);
                pointOptionEnd = PointOption.point((int) (size.width * startPercentage), anchor);
                break;
            default:
                throw new IllegalArgumentException("swipeElementIOS(): dir: '" + dir + "' NOT supported");
        }

        // execute swipe using TouchAction
        try {
            new TouchAction(driver)
                .press(pointOptionStart)
                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration)))
                .moveTo(pointOptionEnd)
                .release().perform();
        } catch (Exception e) {
            System.err.println("swipeElementIOS(): TouchAction FAILED\n" + e.getMessage());
            return;
        }
    }

    //ฟังก์ชั่น Delay รอคำสั่งอื่นหลังจากทำคำสั่งก่อนหน้า หรือก่อนจะขึ้นคำสั่งใหม่
    public void delay(Integer second) throws InterruptedException {
        Thread.sleep(second);
    }

    public static void takePhoto(String caption) throws Exception {
        driver.label(caption);
    }

    /************************************/
    /** สามารถใช้ได้กับการทดสอบบน Localเท่านั้น**/
    //ฟังก์ชั่น ใช้สำหรับอัด VDO
    public void startRecordingScreen() {
        driver.startRecordingScreen(new IOSStartScreenRecordingOptions()
        .withVideoType("h264"));
    }

    //ฟังก์ชั่น ใช้สำหรับหยุดอัด VDO
    public void stopRecordScreen() throws Exception {
        String video = driver.stopRecordingScreen();

        byte[] decodedVideo = Base64.decodeBase64(video);
        String fileName = UUID.randomUUID().toString();
        System.out.println("VDO name: " + fileName);
        FileUtils.writeByteArrayToFile(new File(fileName + ".mp4"), decodedVideo);
    }
}
