package com.microsoft.utils;

public enum Direction {
    DOWN, UP, LEFT, RIGHT;
}
