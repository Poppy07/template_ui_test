package com.microsoft.utils;

import com.microsoft.utils.MainSetupTest;


import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;

public class TestListener extends RunListener {
    /** 
     * Called before any tests have been run.
     * */
    public void testRunStarted(Description description) throws Exception {
        System.out.println("**********START ALL TEST**********");
        System.out.println("I am in onStart " + description.testCount() + " number of tests");
        System.out.println();
    }
 
    public void testRunFinished(Result result) throws Exception {
        System.out.println("I am in onFinish " + result.getRunCount() + " number of tests");
        System.out.println("**********FINISH ALL TEST**********");
    }
 
    public void testStarted(Description description) throws java.lang.Exception {
        System.out.println("================ START TEST CASE ================");
        System.out.println("Starting execution of test case : " + description.getMethodName());
    }
 
    public void testFinished(Description description) throws java.lang.Exception {
        System.out.println(description.getMethodName() + " test is succeed.           FINISH");
        System.out.println("================ FINISH TEST CASE ==============");
        System.out.println();
    }

    public void testFailure(Failure failure) throws java.lang.Exception {
        System.out.println(failure.getMessage() + " test is failed.          FAILED");
        System.out.println("================ FAILED TEST CASE ===============");
        System.out.println();
        //MainSetupTest.takePhoto("Failure");
    }
 
    /**
     *  Called when a test will not be run, generally because a test method is annotated with Ignore.
     * */
    public void testIgnored(Description description) throws java.lang.Exception {
        System.out.println("Execution of test case ignored : "+ description.getMethodName());
    }
}

