package com.microsoft.altframeworktraining;

import static org.junit.Assert.fail;

import com.microsoft.utils.Direction;
import com.microsoft.utils.MainSetupTest;
import com.microsoft.utils.MyRunner;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.junit.Assert;
import org.openqa.selenium.Keys;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import io.appium.java_client.ios.IOSElement;

//StartIOSAppTest
@RunWith(MyRunner.class)
public class LoginTest extends MainSetupTest {

   // EXAMPLE TEST CASE
    @Test
    public void TC_EMPTY_Test() throws InterruptedException {
        resetLogin();
        
        //กรอก mobile number
        MobileElement mobile = driver.findElementByAccessibilityId("Mobile number");
        mobile.sendKeys("", Keys.ENTER);

        //กรอก password
        MobileElement password = driver.findElementByAccessibilityId("password_textfield");
        password.sendKeys("", Keys.ENTER);
        
        //กดปุ่ม login
        driver.findElementByAccessibilityId("Sign_in_button").click();
        Thread.sleep(5000);

        //เช็ค wording ที่ได้จาก popup
        try {
            String answer = driver.findElement(By.id("message_lbl")).getText();
            Assert.assertEquals("Please enter your mobile number", answer);
        } catch (Exception e) {
            fail("Can't find your element");
        }
    }

    @Test
    public void TC_EMPTY_REFACTOR_Test() throws InterruptedException {
        //REFACTOR
        resetLogin();
        
        //1. กรอก mobile number
        //2. กรอก password
        //3. กดปุ่ม login
        //4. เช็ค wording ที่ได้จาก popup "Please enter your mobile number"
    }

    public void resetLogin() throws InterruptedException {
        driver.switchTo().alert().accept();
        driver.switchTo().alert().accept();
        driver.findElementByXPath("//XCUIElementTypeButton[@name=\"Sign_in_button\"]").click();
    }
}
